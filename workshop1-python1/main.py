### Integrate HTML With Flask
### HTTP verb GET And POST

##Jinja2 template engine
'''
{%...%} conditions,for loops
{{    }} expressions to print output
{#....#} this is for comments
'''
import random
from flask import Flask,redirect,url_for,render_template,request

app=Flask(__name__)

@app.route('/index.html')
def welcome(text=''):
    text=''
    number = random.randint(0,4)
    print(number)

    quotes=["Logic will get you from A to B. Imagination will take you everywhere.", 
    "There are 10 kinds of people. Those who know binary and those who don't.", 
    "There are two ways of constructing a software design. One way is to make it so simple that there are obviously no deficiencies and the other is to make it so complicated that there are no obvious deficiencies.", 
    "It's not that I'm so smart, it's just that I stay with problems longer.",
    "It is pitch dark. You are likely to be eaten by a grue."] # string list
    text=quotes[number]

    print(text)

    return render_template('index.html',quote=text)

    



if __name__=='__main__':
    app.run()